<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' =>    'makan ayam',
            'content' => 'di pinggir jalan',
            'category' => 'ke 75'
        ]);
    }
}
